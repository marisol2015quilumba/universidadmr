from django.db import models

# Create your models here.
class CarreraLMQY(models.Model):
    idCarreraLMQY = models.AutoField(primary_key=True)
    nombreCarreraLMQY = models.CharField(max_length=150)
    directorCarreraLMQY = models.CharField(max_length=150)
    logoCarreraLMQY = models.ImageField(upload_to='carrera_logos')
    duracionCarreraLMQY = models.IntegerField()
    modalidadCarreraLMQY = models.CharField(max_length=50)

    def __str__(self):
        return self.nombreCarreraLMQY

class CursoLMQY(models.Model):
    idCursoLMQY = models.AutoField(primary_key=True)
    nivelCursoLMQY = models.CharField(max_length=50)
    descripcionCursoLMQY = models.TextField()
    aulaCursoLMQY = models.CharField(max_length=50)
    fechaInicioCursoLMQY = models.DateField()
    horasClaseSemanaCursoLMQY = models.IntegerField()
    carreraLMQY = models.ForeignKey(CarreraLMQY, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.descripcionCursoLMQY

class AsignaturaLMQY(models.Model):
    idAsignaturaLMQY = models.AutoField(primary_key=True)
    nombreAsignaturaLMQY = models.CharField(max_length=100)
    creditoAsignaturaLMQY = models.IntegerField()
    fechaInicioAsignaturaLMQY = models.DateField()
    fechaFinalizacionAsignaturaLMQY = models.DateField()
    profesorAsignaturaLMQY = models.CharField(max_length=100)
    silaboAsignaturaLMQY = models.FileField(upload_to='silabos')
    descripcionAsignaturaLMQY = models.TextField()
    horarioAsignaturaLMQY = models.CharField(max_length=100)
    cursoLMQY = models.ForeignKey(CursoLMQY, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.nombreAsignaturaLMQY
