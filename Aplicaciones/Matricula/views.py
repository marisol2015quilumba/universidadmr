from django.shortcuts import render, redirect
from django.core.files.storage import default_storage
from django.conf import settings
from django.contrib import messages
from .models import CarreraLMQY
from .models import CursoLMQY
from .models import AsignaturaLMQY

# Create your views here.
def listadoCarreras(request):
    carrerasBdd = CarreraLMQY.objects.all()
    return render(request, 'listadoCarreras.html', {'carreras': carrerasBdd})

def guardarCarrera(request):
    if request.method == 'POST':
        nombre_carrera = request.POST.get("nombre_carrera")
        director_carrera = request.POST.get("director_carrera")
        duracion_carrera = request.POST.get("duracion_carrera")
        modalidad_carrera = request.POST.get("modalidad_carrera")

        logo_carrera = request.FILES.get("logo_carrera")

        carrera = CarreraLMQY.objects.create(
            nombreCarreraLMQY=nombre_carrera,
            directorCarreraLMQY=director_carrera,
            duracionCarreraLMQY=duracion_carrera,
            modalidadCarreraLMQY=modalidad_carrera,
            logoCarreraLMQY=logo_carrera
        )

        messages.success(request, 'CARRERA GUARDADA EXITOSAMENTE')
        return redirect('/')

def eliminarCarrera(request, id_carrera):
    try:
        carrera_eliminar = CarreraLMQY.objects.get(idCarreraLMQY=id_carrera)
        carrera_eliminar.delete()
        messages.success(request, 'CARRERA ELIMINADA EXITOSAMENTE')
    except CarreraLMQY.DoesNotExist:
        messages.error(request, 'La carrera no existe')

    return redirect('/')

def editarCarrera(request, id_carrera):
    try:
        carrera_editar = CarreraLMQY.objects.get(idCarreraLMQY=id_carrera)
        carreras_bdd = CarreraLMQY.objects.all()
        return render(request, 'editarCarrera.html', {'carrera': carrera_editar, 'carreras': carreras_bdd})
    except CarreraLMQY.DoesNotExist:
        messages.error(request, 'La carrera no existe')
        return redirect('/')

def procesarActualizacionCarrera(request):
    if request.method == 'POST':
        id_carrera = request.POST.get("id_carrera")
        nombre_carrera = request.POST.get("nombre_carrera")
        director_carrera = request.POST.get("director_carrera")
        duracion_carrera = request.POST.get("duracion_carrera")
        modalidad_carrera = request.POST.get("modalidad_carrera")

        logo_carrera = request.FILES.get('logo_carrera')

        try:
            carrera_editar = CarreraLMQY.objects.get(idCarreraLMQY=id_carrera)

            if logo_carrera is not None:
                carrera_editar.logoCarreraLMQY = logo_carrera

            carrera_editar.nombreCarreraLMQY = nombre_carrera
            carrera_editar.directorCarreraLMQY = director_carrera
            carrera_editar.duracionCarreraLMQY = duracion_carrera
            carrera_editar.modalidadCarreraLMQY = modalidad_carrera
            carrera_editar.save()

            messages.success(request, 'CARRERA ACTUALIZADA EXITOSAMENTE')
        except CarreraLMQY.DoesNotExist:
            messages.error(request, 'La carrera no existe')

    return redirect('/')
