from django.contrib import admin
from .models import  CarreraLMQY
from .models import  CursoLMQY
from .models import  AsignaturaLMQY

# Register your models here.
admin.site.register(CarreraLMQY)
admin.site.register(CursoLMQY)
admin.site.register(AsignaturaLMQY)
