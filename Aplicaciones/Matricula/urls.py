from django.urls import path
from . import views

urlpatterns = [
 path('', views.listadoCarreras),
    path('guardarCarrera/', views.guardarCarrera),
    path('eliminarCarrera/<int:id_carrera>/', views.eliminarCarrera),
    path('editarCarrera/<int:id_carrera>/', views.editarCarrera),
    path('procesarActualizacionCarrera/', views.procesarActualizacionCarrera),
    

]
